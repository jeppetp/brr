#!/usr/bin/env python3
import sys
from pathlib import Path

# fiddle with python path so we can import brr
sys.path.append(str(Path(__file__).parent.parent))

import brr


def test_render():
    """Check some very basic assumptions about the output.
    Kinda random numbers in here."""
    result = brr.render("./img/lena.png")
    assert isinstance(result, str)
    assert len(result) > 40
    assert result.count("\n") > 10
    assert "⣿" in result
